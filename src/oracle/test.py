import json

from web3 import Web3, HTTPProvider
from web3.eth import Account

'''
addd upd liquidity 
enable robust left right
commit roight 1 eth
'''


# Sends transaction. If `function` in kwargs, sends contract call.
# If `receipt` is `True` - returns receipt, else - transaction hash.
def send_transaction(web3: Web3, account: Account, receipt=True, revert=False, \
                     **kwargs) -> dict:
    tx = {
        'from': account.address,
        'nonce': web3.eth.getTransactionCount(account.address),

        'gasPrice': 20000000000
    }

    # Custom parameters.
    tx.update(kwargs)

    try:
        # Contract call.
        if 'function' in tx:
            # Geth.
            gas_price = tx.pop('gasPrice')

            tx = tx.pop('function').buildTransaction(tx)

            tx['gasPrice'] = gas_price

        else:
            tx['gas'] = web3.eth.estimateGas(tx)

    except Exception as exc:
        # Should revert.
        if revert:
            return None

        raise exc

    # If not reverted.
    assert not revert, \
        f'Transaction should be reverted:\n{json.dumps(tx, indent=4, sort_keys=True)}.'

    tx_hash = web3.eth.sendRawTransaction(account.sign_transaction(tx).rawTransaction)

    if receipt:
        return web3.eth.waitForTransactionReceipt(tx_hash)

    return tx_hash


# Sends transaction and checks that status == `status`,
# If `receipt` is `True` - returns receipt, else - transaction hash.
def send_transaction_and_check(web3: Web3, account: Account, status=1, receipt=True, \
                               revert=False, **kwargs) -> dict:
    try:
        tx_receipt = send_transaction(web3, account, receipt, revert, **kwargs)

    except Exception as exc:
        raise Exception(f'Cannot execute transaction: {exc}')

    if revert:
        assert tx_receipt is None, \
            'Transaction should be reverted: {tx_reseipt}.'

        return None

    if receipt:
        # Checks only of we wait receipt.
        assert int(tx_receipt.status) == status, \
            f'Transaction status mismatch: {tx_receipt.status}'

    return tx_receipt


def setLiquidity(bridge_left, bridge_right, owner_account, valueInWei):
    """
    Set the liquidity on `bridge_left` and `bridge_bridge`
    to `value` (in ether) using `owner_account`
    """
    web3_left = bridge_left.web3
    web3_right = bridge_right.web3

    send_transaction_and_check(web3_right, owner_account,
                               function=bridge_right.functions.addLiquidity(),
                               value=valueInWei)

    assert bridge_right.functions.getLiquidityLimit().call() >= valueInWei

    send_transaction_and_check(web3_left, owner_account,
                               function=bridge_left.functions.updateLiquidityLimit(valueInWei),
                               value=0)

    assert bridge_left.functions.getLiquidityLimit().call() >= valueInWei


url = "http://sokol.poa.network"
from config import PRIVKEY as priv_key
from json import loads
from contract_abi import LEFT_ABI, RIGHT_ABI

web3 = Web3(HTTPProvider(url))
acc = web3.eth.account.from_key(priv_key)

left = web3.eth.contract(address='0xeaBd5e26e8a2C97b090680B2d2dbd033ADf17629', abi=LEFT_ABI)
right = web3.eth.contract(address='0x9391AE4CfcC4Fb5B27C3b816D633a05Ff12935a7', abi=RIGHT_ABI)

print('setLiquidity')
setLiquidity(left, right, acc, web3.toWei('10', 'ether'))

print('enable')
send_transaction_and_check(web3, acc,
                           function=left.functions.enableRobustMode(),
                           value=0)
send_transaction_and_check(web3, acc,
                           function=right.functions.enableRobustMode(),
                           value=0)

print('sent to left')
send_transaction(web3, acc, to=left.address, value=web3.toWei('0.1', 'ether'))
import time
print('sleep 10 sec')
time.sleep(10)
print('send to right')
send_transaction(web3, acc, to=right.address, value=web3.toWei('0.1', 'ether'))