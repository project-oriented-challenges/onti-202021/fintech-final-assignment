pragma solidity ^0.7.6;

contract AddressRecoveryTool {
    function wrapped(address _a, uint256 _v, bytes32 _h) pure internal returns(bytes32)  {
        return keccak256(abi.encodePacked('\x19Ethereum Signed Message:\n32', getRobustModeMessage(_a, _v, _h)));
    }

    function getRobustModeMessage(address recipient, uint256 amount, bytes32 id) pure public returns (bytes32){
        return keccak256(abi.encodePacked(recipient, amount, id));
    }

    function recover(address _rcpt, uint256 _value, bytes32 _txhash, uint8 _v, bytes32 _r, bytes32 _s) pure internal returns(address) {
        return ecrecover(wrapped(_rcpt, _value, _txhash), _v, _r, _s);
    }
}
