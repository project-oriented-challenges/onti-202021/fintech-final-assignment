from redis import Redis
from config import REDIS_CONFIG
from typing import Union


redis_db = Redis(**REDIS_CONFIG)
redis_db.ping()

LEFT_BLOCK_NUMBER = 'oracle::left_block_number'
RIGHT_BLOCK_NUMBER = 'oracle::right_block_number'

def _set(key: Union[int, str, bytes], value: Union[int, str, bytes]):
    redis_db.set(key, value)

def _get(key: Union[int, str, bytes]) -> bytes:
    return redis_db.get(key)

def get_left_block() -> int:
    """
    Restore block number value from database
    """
    return int(_get(LEFT_BLOCK_NUMBER) or 0)

def get_right_block() -> int:
    """
    Restore block number value from database
    """
    return int(_get(RIGHT_BLOCK_NUMBER) or 0)


def set_left_block(block_numer):
    """
    Set block_number to database
    """
    _set(LEFT_BLOCK_NUMBER, block_numer)


def set_right_block(block_numer):
    """
    Set block_number to database
    """
    _set(RIGHT_BLOCK_NUMBER, block_numer)
