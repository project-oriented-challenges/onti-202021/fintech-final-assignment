import argparse
import logging
from json import dump

from blockchain import (NotEnoughFunds, account, compile_contract,
                        deploy_contract, normal_addr, web3_right, web3_left,
                        LEFT_CONNECTED, RIGHT_CONNECTED)
from utils import exit_with_message

from config import (CONTRACT_RIGHT, CONTRACT_LEFT, CONTRACT_VALIDATORS,
                    THRESHOLD, VALIDATORS, LEFT_RPCURL, RIGHT_RPCURL)


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--verify', action='store_true',
        help='if true then verify the contracts',
    )

    return parser.parse_args()


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    if not LEFT_CONNECTED:
        exit_with_message(f"Cannot connect to left rpc: {LEFT_RPCURL}")
    if not RIGHT_CONNECTED:
        exit_with_message(f"Cannot connect to right rpc: {RIGHT_RPCURL}")


    args = parse_arguments()

    OWNER = account.address
    VERIFY = args.verify

    VALIDATORS = tuple(map(normal_addr, VALIDATORS))
    
    validator_left_deploy_tx = deploy_contract(
        web3=web3_left,
        args=(VALIDATORS, THRESHOLD, OWNER),
        verify=VERIFY,
        **CONTRACT_VALIDATORS,
    )
    VALIDATOR_LEFT_ADDRESS = validator_left_deploy_tx['contractAddress']
    print(f"#1 [LEFT] Validators Set deployed at {VALIDATOR_LEFT_ADDRESS}")

    left_bridge_deploy_tx = deploy_contract(
        web3=web3_left,
        args=(VALIDATOR_LEFT_ADDRESS, OWNER,),
        verify=VERIFY,
        **CONTRACT_LEFT
    )
    
    LEFT_BRIDGE_ADDRESS = left_bridge_deploy_tx['contractAddress']
    print(f"#2 [LEFT] Bridge deployed at {LEFT_BRIDGE_ADDRESS}")

    validator_right_deploy_tx = deploy_contract(
        web3=web3_right,
        args=(VALIDATORS, THRESHOLD, OWNER),
        verify=VERIFY,
        **CONTRACT_VALIDATORS,
    )
    VALIDATOR_RIGHT_ADDRESS = validator_right_deploy_tx['contractAddress']
    print(f"#3 [RIGHT] Validators Set deployed at {VALIDATOR_RIGHT_ADDRESS}")

    right_bridge_deploy_tx = deploy_contract(
        web3=web3_right,
        args=(VALIDATOR_RIGHT_ADDRESS, OWNER),
        verify=VERIFY,
        **CONTRACT_RIGHT
    )
    RIGHT_BRIDGE_ADDRESS = right_bridge_deploy_tx['contractAddress']
    print(f"#4 [RIGHT] Bridge deployed at {RIGHT_BRIDGE_ADDRESS}")

    LEFT_BLOCK = left_bridge_deploy_tx['blockNumber']
    print(f"#5 [LEFT] Bridge deployed at block {LEFT_BLOCK}")
    
    RIGHT_BLOCK = right_bridge_deploy_tx['blockNumber']
    print(f"#6 [RIGHT] Bridge deployed at block {RIGHT_BLOCK}")
