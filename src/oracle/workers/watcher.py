from multiprocessing import Value
from time import sleep

from eth_typing import Address
from contract_functions import isRobustMode
from blockchain import get_prepared_web3, account
from .constants import getLogger, Queue
from crypto import prepareSignature
from config import REQUIRED_CONFIRMATIONS


def event_watcher(rpc_url_home: str, rpc_url_foreign: str, contract_address_left: Address, abi_left: dict,
                  contract_address_right: Address, abi_right: dict, is_running: Value,
                  output_queue: Queue,
                  start_block: int, write_to_bd: callable, side: str) -> None:
    """
    Watches for event forever 
    """
    logger = getLogger(f"{rpc_url_home}-wathcer")

    web3_home = get_prepared_web3(rpc_url_home)
    web3_foreign = get_prepared_web3(rpc_url_foreign)

    if side == 'right':
        contract_left = web3_foreign.eth.contract(address=contract_address_left, abi=abi_left)
        contract_right = web3_home.eth.contract(address=contract_address_right, abi=abi_right)
        contract = contract_right
    else:
        contract_left = web3_home.eth.contract(address=contract_address_left, abi=abi_left)
        contract_right = web3_foreign.eth.contract(address=contract_address_right, abi=abi_right)
        contract = contract_left
    latest_block = web3_home.eth.blockNumber

    logger.info(f"{REQUIRED_CONFIRMATIONS} -- REQUIRED_CONFIRMATIONS")
    logger.info(f"{start_block} -> {latest_block}")

    while is_running.value:
        latest_block = web3_home.eth.blockNumber
        while start_block > latest_block:
            latest_block = web3_home.eth.blockNumber
            sleep(0.1)
        if side == 'right' and isRobustMode(contract_left):
            filter_ = contract.events.commitsCollected.createFilter(fromBlock=start_block, toBlock=latest_block)
            events = filter_.get_all_entries()
            if events:
                logger.info(f'Found {len(events)} event(s) commits collected')

            for event in events:
                function = contract_left.functions.applyCommits
                id_, commits = '0x' + event['args']['id'].hex(), event['args']['commits']
                logger.info(f'{id_} {commits}: commits collected')
                try:
                    oracle = contract_right.functions.getAssignee(id_).call()
                except Exception as e:
                    logger.error(f"{contract_right.functions.getAssignee(id_)}, {contract_right.address}")
                    raise e

                if account.address != oracle:
                    logger.info(f'Needed oracle: {oracle}, I\'am: {account.address}')
                    continue
                recipient, amount = contract_right.functions.getTransferDetails(id_).call()
                r = []
                s = []
                v = []
                for i in range(commits):
                    r_, s_, v_ = contract_right.functions.getCommit(id_, i).call()
                    r.append(r_)
                    s.append(s_)
                    v.append(v_)
                args = recipient, amount, id_, r, s, v
                logger.info(f'Args of event: {args}: commits collected')
                dict_for_queue = {'args': args, 'side': 'left', 'function': function}
                output_queue.put(dict_for_queue)
                logger.info(f"Found event {event}.")

        logger.info(f'Found block new block: {latest_block}')
        to_check_block = latest_block - REQUIRED_CONFIRMATIONS
        if to_check_block < start_block:
            continue
        logger.info(f"{start_block} -> {to_check_block}")
        filter_ = contract.events.bridgeActionInitiated.createFilter(fromBlock=start_block, toBlock=to_check_block)
        events = filter_.get_all_entries()
        if events:
            logger.info(f'Found {len(events)} event(s)')

        for event in events:
            if side == 'right' and isRobustMode(contract_left):
                function = contract_right.functions.registerCommit
                args = event['args']['recipient'], event['args']['amount'], event['transactionHash'], *prepareSignature(
                    account, event['args']['recipient'], event['args']['amount'], event['transactionHash'])
                dict_for_queue = {'args': args, 'side': 'right', 'function': function}
            else:
                if side == 'right':
                    function = contract_left.functions.commit
                    side_ = 'left'
                else:
                    function = contract_right.functions.commit
                    side_ = 'right'
                args = event['args']['recipient'], event['args']['amount'], event['transactionHash']
                dict_for_queue = {'args': args, 'side': side_, 'function': function}
            output_queue.put(dict_for_queue)
            logger.info(f"Found event {event}.")


        write_to_bd(to_check_block + 1)
        start_block = to_check_block + 1


'''
    filter_ = contract.events.bridgeActionInitiated.createFilter(fromBlock=start_block, toBlock=latest_block)
    events = filter_.get_all_entries()
    if events:
        logger.info(f'Found {len(events)} event(s)')

    for event in events:
        args = event['args']['id'], event['args']['commits']

        output_queue.put(
            {'args': args}
        )
        logger.info(f"Found event {event}.")
    write_to_bd(latest_block + 1)
    start_block = latest_block + 1
'''
