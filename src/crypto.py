from eth_account.messages import encode_defunct
from blockchain import account
from web3 import Web3
from web3 import Account
from eth_account.messages import (
    SignableMessage,
    _hash_eip191_message,
)
from eth_abi.packed import encode_single_packed, encode_abi_packed

def prepareSignature(account, recipient, value, txHash):
    """
    Signs message and returns r s v
    """
    msg = Web3.solidityKeccak(['address', 'uint256', 'bytes32'], [recipient, value, txHash])
    #msg = encode_abi_packed(['address', 'uint256', 'bytes32'], [recipient, value, txHash])
    wrapped_msg = encode_defunct(msg)
    signed = account.sign_message(wrapped_msg)
    v, r, s = signed.v, signed.r, signed.s
    return r, s, v

if __name__ == '__main__':
    recipient = Web3.toChecksumAddress("0xF91296B9d7699d800c2Ba0a80e755972a9DA7C34")
    value = Web3.toWei(10, 'ether')
    txhash = "0xac21bd9c034f02266f79ac064560ea1ed3e8b8ff6491f0f8e433e9122ae4e804"
    key = "0x930650054b50608dafb24c08b71f46bf8605de83d9ad5502cad0c6159dd074bf"

    r, s, v = prepareSignature(account, recipient, value, txhash)
    print(account.address)
    print(recipient, value, txhash)
    print(r, s, v)
