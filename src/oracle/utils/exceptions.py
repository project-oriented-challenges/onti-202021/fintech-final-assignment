class AccountNotValidator(Exception):
    def __init__(self, address, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.address = address

class EventWasAlreadySent(Exception):
    pass
