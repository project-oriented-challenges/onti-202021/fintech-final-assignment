import time
from multiprocessing import Value
from queue import Empty as EmptyException

from eth_typing import Address

from blockchain import get_prepared_web3, get_deployed_contract
from .constants import builder_logger, Queue
from contract_functions import isValidator
from utils.exceptions import AccountNotValidator, EventWasAlreadySent
from web3 import Web3


def buildCommitTransaction(function, args):
    """
    Check validator account and build the transaction
    """
    web3: Web3 = function.web3
    contract_address = function.address
    contract_abi = function.contract_abi
    
    contract = get_deployed_contract(web3, contract_address, contract_abi)

    validator_address = web3.eth.defaultAccount
    if not isValidator(contract, validator_address):
        raise AccountNotValidator(validator_address)

    try:
        tx = function(*args).buildTransaction()
    except ValueError as e:
        builder_logger.info(f"{args}")
        raise EventWasAlreadySent(*e.args)
    tx['gas'] *= 4
    return tx


def tx_builder_worker(is_running: Value, input_queue: Queue, left_output_queue: Queue,\
     right_output_queue: Queue):
    # Processing.
    while is_running.value:
        try:
            job = input_queue.get_nowait()
            builder_logger.info(f"Got job {job}")
            side, function, args = job['side'], job['function'], job['args']

        # To prevend blocking when is_running is False.
        except EmptyException:
            # Wait 100 ms.
            time.sleep(0.1)
            # Again.
            continue

        try:
            # Build transaction.
            tx = buildCommitTransaction(function, args)
        except AccountNotValidator as e:
            builder_logger.error(f'Account is not validator! {e.address}')
            # put job back
            input_queue.put(job)
            time.sleep(1)
            continue

        except EventWasAlreadySent as e:
            builder_logger.error(f"Transaction was already sent")
            continue
        
        builder_logger.info(f'Put builded transaction {tx}')
        
        if side == 'left':
            left_output_queue.put({'tx': tx})
        
        if side == 'right':
            right_output_queue.put({'tx': tx})
