// SPDX-License-Identifier: MIT

pragma solidity ^0.7.6;

import "./Ownable.sol";


contract BaseBridgeValidators is Ownable {
    address public constant F_ADDR = 0xFFfFfFffFFfffFFfFFfFFFFFffFFFffffFfFFFfF;
    
    
    event ValidatorAdded(address indexed validator);
    event ValidatorRemoved(address indexed validator);
    event RequiredSignaturesChanged(uint256 requiredSignatures);
    
    uint public m_validatorCount;
    uint m_required_signatures;
    
    mapping (bytes32 => address) validator_list;
    
    
    function getThreshold() public view returns (uint256){
        return m_required_signatures;
    }
    
    function changeThreshold(uint256 thresh) public onlyOwner {
        _changeThreshold(thresh);
        emit RequiredSignaturesChanged(thresh);
    }
    
    function _changeThreshold(uint256 thresh) internal {
        require(m_validatorCount >= thresh, "# of validators < than requested number");
        require(thresh != 0, "Zero _requiredSignatures");
        m_required_signatures = thresh;
    }
    
    
    function isValidator(address _validator) public view returns (bool) {
        return _validator != F_ADDR && _getNextValidator(_validator) != address(0);
    }
    
    /*
        Set next validator for `_prevValidator` to `_validator`
    */
    function _setNextValidator(address _prevValidator, address _validator) internal{
        validator_list[keccak256(abi.encodePacked("validatorList", _prevValidator))] = _validator;
    }
    
    /*
        Return next validator for `_validator`. 
        If `_validator` is the last in the list,
        return 0xFF..FF
    */
    function _getNextValidator(address _validator) internal view returns (address) {
        return validator_list[keccak256(abi.encodePacked("validatorList", _validator))];
    }
    
    /*
        Delete `_validator` from the storage
    */
    function _deleteValidatorFromList(address _validator) internal {
        delete validator_list[keccak256(abi.encodePacked("validatorList", _validator))];
    }
    
    /*
        Return previous validator for `_validator` in the validator list
    */
    function _getPrevValidator(address _validator) internal view returns (address) {
        address index = F_ADDR;
        address nextIndex = _getNextValidator(index);
        
        require(nextIndex != address(0), "there is no such validator in list");
        
        while (nextIndex != _validator) {
            
            index = nextIndex;
            nextIndex = _getNextValidator(index);
            
            require(nextIndex != F_ADDR && nextIndex != address(0), "there is no such validator in list"); 
        }
        return index;
    }
    
    /*
        Add `_validator` to the begging of the validator list
    */
    function _addValidator(address _validator) internal {
        // check preconditions
        require(!isValidator(_validator), "There is already such validator");

        address firstValidator = _getNextValidator(F_ADDR);
        // there is no validators
        if (firstValidator == address(0)){
            firstValidator = F_ADDR;
        }
        _setNextValidator(_validator, firstValidator);
        _setNextValidator(F_ADDR, _validator);
        m_validatorCount++;

    }
    
    /*
        Remove `_validator` from the validator list
    */
    function _removeValidator(address _validator) internal {
        // check preconditions
        require(m_validatorCount > m_required_signatures, "invalid number of validators");
        require(isValidator(_validator), "No such validator");

        address prevValidator = _getPrevValidator(_validator);
        address nextValidator = _getNextValidator(_validator);
        
        _setNextValidator(prevValidator, nextValidator);
        _deleteValidatorFromList(_validator);
        m_validatorCount--;
    }
    
    /*
        Return list of all validators
    */
    function _validatorList() internal view returns (address[] memory) {
        address[] memory list = new address[](m_validatorCount);
        uint256 counter = 0;
        address nextValidator = _getNextValidator(F_ADDR);
        
        if (nextValidator == address(0)){
            return list;
        }

        while (nextValidator != F_ADDR) {
            list[counter] = nextValidator;
            nextValidator = _getNextValidator(nextValidator);
            counter++;

            require(nextValidator != address(0));
        }

        return list;
    }
    
}


contract BridgeValidators is BaseBridgeValidators{
    constructor (address[] memory _validators, uint256 _requiredSignatures, address _owner) {
        _setOwner(_owner);
        
        // add validator from validator list
        for (uint i = 0; i < _validators.length; ++i){
            _addValidator(_validators[i]);
            emit ValidatorAdded(_validators[i]);   
        }
        
        // set threshold
        _changeThreshold(_requiredSignatures);
        emit RequiredSignaturesChanged(_requiredSignatures);
    }
    
    
    function addValidator(address newvalidator) public onlyOwner {
        _addValidator(newvalidator);
        emit ValidatorAdded(newvalidator);
    }
    
    function removeValidator(address validator) public onlyOwner {
        _removeValidator(validator);
        emit ValidatorRemoved(validator);
    }
    
    function getValidators() public view returns (address[] memory) {
        return _validatorList();
    }
    
}