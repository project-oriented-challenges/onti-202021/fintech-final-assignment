from queue import Empty as EmptyException
from multiprocessing import Value
from .constants import sender_logger as logger, Queue
from blockchain import get_prepared_web3, account

import time
class LowBalance(Exception):
    pass

def tx_sender_worker(rpc_url: str, gasPrice: int, is_running: Value,
                     input_queue: Queue, output_queue: Queue):
    web3 = get_prepared_web3(rpc_url)
    nonce = web3.eth.getTransactionCount(account.address)
    while is_running.value:
        try:
            job = input_queue.get_nowait()
            tx = job['tx']
            logger.info(f'Got transaction. {tx}')

        # To prevend blocking when is_running is False.
        except EmptyException:
            # Wait 100 ms.
            time.sleep(0.1)

            # Again.
            continue

        try:
            # Success.
            tx['gasPrice'] = gasPrice
            while True:
                try:
                    logger.info(f"Send transaction with nonce={nonce}")
                    tx['nonce'] = nonce
                    raw_signed = account.sign_transaction(tx).rawTransaction
                    txHash = web3.eth.sendRawTransaction(raw_signed)
                except ValueError as e:
                    if web3.eth.getBalance(account.address) < tx['gas'] * tx['gasPrice']:
                        raise LowBalance
                    else:
                        nonce = web3.eth.getTransactionCount(account.address)
                        continue
                break

            current_time = time.time()
            logger.info(f"Sent at {txHash}")
            output_queue.put((tx, txHash, current_time))
            nonce += 1

        except Exception as e:
            logger.error('Low balance!')
            input_queue.put(job)
            time.sleep(1)
            continue
            
