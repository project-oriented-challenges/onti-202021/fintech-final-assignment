#!/usr/local/bin/python
import sys
from blockchain import account, web3_left, web3_right, get_deployed_contract, ZERO_ADDRESS
from config import LEFT_ADDRESS, RIGHT_ADDRESS
from oracle.contract_abi import LEFT_ABI, RIGHT_ABI
from oracle.contract_functions import (
    isRobustMode, getTransferDetails, 
    getCommit, custom_getCommitsAmount,
    applyCommits
    )
import logging

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    contract_left = get_deployed_contract(web3_left, LEFT_ADDRESS, LEFT_ABI)
    contract_right = get_deployed_contract(web3_right, RIGHT_ADDRESS, RIGHT_ABI)

    assert isRobustMode(contract_left), "Robust mode on left is off"
    assert isRobustMode(contract_right), "Robust mode on right is off"

    txHash = sys.argv[1]

    receiver, amount = getTransferDetails(contract_right, txHash)
    logging.info(f"receiver, amount: {receiver, amount}")
    assert receiver != ZERO_ADDRESS and amount != 0, f"There is no such action with id={txHash}"

    commits = custom_getCommitsAmount(contract_right, txHash)
    logging.info(f"Found {commits} commits")
    rs, ss, vs = [], [], []

    for index in range(commits):
        r, s, v = getCommit(contract_right, txHash, index)
        logging.info(f"r, s, v: {r, s, v}")
        rs.append(r)
        ss.append(s)
        vs.append(v)
    
    tx = applyCommits(web3_left, contract_left, account, receiver, amount, txHash, rs, ss, vs)
    print(f"{tx['transactionHash'].hex()} executed")
