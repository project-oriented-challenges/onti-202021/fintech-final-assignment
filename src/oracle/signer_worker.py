from multiprocessing import Value, cpu_count
from queue import Queue
from threading import Thread
from contract_functions import isRobustMode
from contract_abi import LEFT_ABI, RIGHT_ABI
from workers import (
    event_watcher,
    tx_builder_worker,
    tx_sender_worker,
    receipt_checker_worker,
)

from config import (
    LEFT_RPCURL, LEFT_ADDRESS, LEFT_START_BLOCK, LEFT_GASPRICE,
    RIGHT_RPCURL, RIGHT_ADDRESS, RIGHT_START_BLOCK, RIGHT_GASPRICE,
)
from utils import set_left_block, set_right_block, get_right_block, get_left_block


def get_block(side):
    if side == 'left':
        database_block = get_left_block()
        return max(database_block, LEFT_START_BLOCK)
    else:
        database_block = get_right_block()
        return max(database_block, RIGHT_START_BLOCK)


class OracleManager:
    def __init__(self, builders=cpu_count()):        
        self.web3_url_left = LEFT_RPCURL
        self.contract_address_left = LEFT_ADDRESS
        self.abi_left = LEFT_ABI

        self.web3_url_right = RIGHT_RPCURL
        self.contract_address_right = RIGHT_ADDRESS
        self.abi_right = RIGHT_ABI

        self.gasPrice_left = LEFT_GASPRICE
        self.gasPrice_right = RIGHT_GASPRICE
        self.start_block_left = get_block('left')
        self.start_block_right = get_block('right')

        self.set_left_block = set_left_block

        self.set_right_block = set_right_block

        # Loop flag.
        self.__is_sender_running = Value('i', True)
        self.__is_builders_running = Value('i', True)
        self.__is_watcher_running = Value('i', True)
        self.__is_receipt_checker_running = Value('i', True)

        # Queues.
        self.builders_input = Queue()
        
        self.sender_left_input = Queue()
        self.sender_right_input = Queue()

        self.sender_left_output = Queue()
        self.sender_right_output = Queue()

        # Create left watcher
        watcher_left_args = (
            self.web3_url_left,
            self.web3_url_right,
            self.contract_address_left,
            self.abi_left,
            self.contract_address_right,
            self.abi_right,
            self.__is_watcher_running,
            self.builders_input,
            self.start_block_left,
            self.set_left_block,
            'left'
        )
        self.__watcher_left = Thread(target=event_watcher, args=watcher_left_args, name='watcher-left')

        # Create right watcher
        watcher_right_args = (
            self.web3_url_right,
            self.web3_url_left,
            self.contract_address_left,
            self.abi_left,
            self.contract_address_right,
            self.abi_right,
            self.__is_watcher_running,
            self.builders_input,
            self.start_block_right,
            self.set_right_block,
            'right'
        )
        self.__watcher_right = Thread(target=event_watcher, args=watcher_right_args, name='watcher-right')

        # Builders.
        builders_args = (
            self.__is_builders_running,
            self.builders_input,
            self.sender_left_input,
            self.sender_right_input,
        )

        self.__builders = [
                Thread(target=tx_builder_worker, args=builders_args, name=f'builder-{i}')
                for i in range(builders)
        ]

        # left sender
        sender_left_args = (
            self.web3_url_left,
            self.gasPrice_left,
            self.__is_sender_running,
            self.sender_left_input,
            self.sender_left_output,
        )

        self.__sender_left = Thread(target=tx_sender_worker, args=sender_left_args, name='sender-left')

        # right sender
        sender_right_args = (
            self.web3_url_right,
            self.gasPrice_right,
            self.__is_sender_running,
            self.sender_right_input,
            self.sender_right_output,
        )

        self.__sender_right = Thread(target=tx_sender_worker, args=sender_right_args, name='sender-right')
        

        # Receipt checker.
        receipt_checker_left_args = (
            self.web3_url_left,
            self.__is_receipt_checker_running,
            self.sender_left_output,
            self.sender_left_input,
        )

        self.__receipt_checker_left = Thread(target=receipt_checker_worker, args=receipt_checker_left_args,
                                        name='receipt_checker_left')

        # right Receipt checker.
        receipt_checker_right_args = (
            self.web3_url_right,
            self.__is_receipt_checker_running,
            self.sender_right_output,
            self.sender_right_input,
        )

        self.__receipt_checker_right = Thread(target=receipt_checker_worker, args=receipt_checker_right_args,
                                        name='receipt_checker_right')

    def start(self) -> None:
        self.__watcher_left.start()
        self.__watcher_right.start()

        self.__sender_left.start()
        self.__sender_right.start()
        

        self.__receipt_checker_left.start()
        self.__receipt_checker_right.start()

        for builder in self.__builders:
            builder.start()

    def stop(self) -> None:
        with self.__is_sender_running.get_lock():
            self.__is_sender_running.value = False

        with self.__is_builders_running.get_lock():
            self.__is_builders_running.value = False

        with self.__is_watcher_running.get_lock():
            self.__is_watcher_running.value = False

        with self.__is_receipt_checker_running.get_lock():
            self.__is_receipt_checker_running.value = False

    def join(self) -> None:
        for builder in self.__builders:
            builder.join()
        
        self.__watcher_left.join()
        self.__watcher_right.join()

        self.__sender_left.join()
        self.__sender_right.join()
        

        self.__receipt_checker_left.join()
        self.__receipt_checker_right.join()


    def __enter__(self):
        self.start()

        return self

    def __exit__(self, exc_type, exc_value, traceback) -> None:
        self.stop()
        self.join()
